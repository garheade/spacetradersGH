# Spacetrader GUI
## Getting Started
Create a file `secret.py` with your token:
```python
# secret.py
TOKEN = "[YOUR TOKEN HERE]"
```
Next run to install the requirements
`pip install -r requirements.txt`

To run the website use `python run.py` for now.
